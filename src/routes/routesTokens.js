'use strict';

import ControllerToken from '../controllers/ControllerToken.js';
import ControllerAuth from '../controllers/ControllerAuth.js';

/**
 * Mounts all routes of ControllerToken.
 * @param  {Object} app    Express Server Object
 * @param  {Object} config Configs from config.json
 */
export default function routesTokens(app, config){
    const controllerToken = new ControllerToken(config);
    const controllerAuth = new ControllerAuth(config);
    
    app.get('/tokens', controllerToken.getAllTokens);
    app.get('/tokens/:id', controllerToken.getToken);

    app.post('/tokens', [controllerAuth.auth], controllerToken.postToken);
    app.put('/tokens/:id', [controllerAuth.auth], controllerToken.putToken);
    app.patch('/tokens/:id', [controllerAuth.auth], controllerToken.patchToken);
    app.delete('/tokens/:id', [controllerAuth.auth], controllerToken.deleteToken);
}

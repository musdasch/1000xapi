'use strict';

import ControllerTrade from '../controllers/ControllerTrade.js';

/**
 * Mounts all routes of ControllerTrade.
 * @param  {Object} app    Express Server Object
 * @param  {Object} config Configs from config.json
 */
export default function routesTokens(app, config){
    const controllerTrade = new ControllerTrade(config);
    
    app.get('/trades', controllerTrade.getAllTrades);
}
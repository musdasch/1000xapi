'use strict';

import Web3 from 'web3';

import express from 'express';
import cors from 'cors';

import routesAuth from './../routes/routesAuth.js'
import routesTokens from './../routes/routesTokens.js'
import routesTrades from './../routes/routesTrades.js'

/**
 * Basic API Webserver
 */
export default class Server {

  /**
   * Config object from config.json.
   */
  #config;

  /**
   * Server process in order to stop the server.
   */
  #process;
  
  /**
   * Constructor of the server.
   * @param  {Object} config Config object from config.json.
   */
  constructor(config) {
    this.#config = config;
  }

  /**
   * Starts the server.
   * @return {Promise} Promise if its finished.
   */
  async start() {
    const web3 = new Web3(this.#config.provider);
    const chainId = await web3.eth.net.getId();

    const app = express();

    app.use(express.json());
    app.use(cors({
        origin: '*'
    }));

    routesAuth(app, this.#config);
    routesTokens(app, this.#config);
    routesTrades(app, this.#config);

    app.use((req, res) => {
      res.status(404).json({message: "Not Found"});
    });

    if(this.#config.chainId == chainId) {
      this.#process = await app.listen(
        this.#config.server.port
      );
    } else {
      console.log(
        "Wrong chain ID, it should be 37480 but was "
          + chainId
          + "."
      );
    }
  }

  /**
   * stops the server.
   */
  stop() {
    if(this.#process)
      this.#process.close();
  }
}

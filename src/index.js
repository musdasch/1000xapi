'use strict';

import Server from './servers/Server.js';

import readJsonSync from 'read-json-sync';

/**
 * Main starts the program.
 * @param  {Array} argv   Arguments on terminal.
 */
function main(argv) {
  argv[0] = argv[0] !== undefined ? argv[0] : 'configs/config.json';

  const server = new Server(
    readJsonSync(argv[0])
  );
  server.start();
}
main(process.argv.slice(2));

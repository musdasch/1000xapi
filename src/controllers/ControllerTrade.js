'use strict';

import Controller from './Controller.js';

import RepositoryToken from './../repositorys/RepositoryToken.js';

import Web3 from 'web3';
import BigNumber from 'bignumber.js';

import readJsonSync from 'read-json-sync';

/**
 * Token controller to use in Routes.
 */
export default class ControllerToken extends Controller {

  /**
   * Not used yet.
   */
  #repository;

  /**
   * ABI code of contract.
   */
  #swapContract;

  /**
   * Constructs the token controller.
   * @param  {Object} config Basic configurations
   */
  constructor(config) {
    super();

    this.#repository = new RepositoryToken(config.database);

    const web3 = new Web3(
      new Web3.providers.HttpProvider(config.provider)
    );

    const thousandXSWAPABI = readJsonSync("assets/abi/ThousandXSWAP.json");
    
    this.#swapContract = new web3.eth.Contract(
      thousandXSWAPABI,
      config.thousandXSWAPAddress
    );
  }

  /**
   * Returns all trades as a response.
   * @param  {Object} req Request object.
   * @param  {Object} res Response object.
   * @return {Object}     Returns the ServerResponse object.
   */
  getAllTrades = async (req, res) => {
    const tokens = await this.#repository.findAll();
    const tokenDecimals = new Map();

    tokens.forEach((token) => {
      tokenDecimals.set(token.address, token.decimals);
    });

    try {
      const events = await this.#swapContract.getPastEvents(
        'newSWAP',
        {fromBlock: 1815472}
      );

      const trades = [];

      events.forEach((event) => {
        let amountInDecimal = tokenDecimals.get(event.returnValues.tokenIn);
        let amountOutDecimal = tokenDecimals.get(event.returnValues.tokenOut);

        if(typeof amountInDecimal != 'number') {
          amountInDecimal = 12;
        }

        if(typeof amountOutDecimal != 'number') {
          amountOutDecimal = 12;
        }


        trades.unshift({
          blockNumber: event.blockNumber,
          sender: event.returnValues.sender,
          tokenIn: event.returnValues.tokenIn,
          tokenOut: event.returnValues.tokenOut,
          amountIn: this.#toDecimal(event.returnValues.amountIn, amountInDecimal),
          amountOut: this.#toDecimal(event.returnValues.amountOut, amountOutDecimal)
        })
      });

      return res.status(200).json(trades);
    } catch(e) {
      this.handleResponsError(e, res);
      return;
    }
  };

  /**
   * Calculates a desimal from big int.
   * @param  {Number} value    Big nummber from blockchain.
   * @param  {Number} decimals Decimal places.
   * @return {Number}          Number as decimal.
   */
  #toDecimal(value, decimals) {
    const bigValue = new BigNumber(10);
    return bigValue.pow(-decimals).times(value).toNumber();
  }
}

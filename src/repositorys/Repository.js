'use strict';

import Database from 'mysql-promise-wrapper';

/**
 * Basic Repository class.
 */
export default class Repository {

  /**
   * Holds the database Object in order to connect to it.
   */
  #database;

  /**
   * Constructor of the Token Repository 
   * @param {Object} options - options to set up the database connection.
   */
  constructor(options) {
    this.#database = new Database({
      host     : options.host,
      user     : options.user,
      password : options.password,
      database : options.database
    });
  }

  /**
   * In order to query the database.
   * @param  {String}   sql           SQL query string.
   * @param  {Array}    [options=[]]  Values for prepared statement.
   * @return {Promise}                Returns Promise.
   */
  query(sql, options) {
    options = options !== undefined ? options : [];

    return this.#database.connection((connection) => {
      return connection.query(sql, options);
    });
  }
}

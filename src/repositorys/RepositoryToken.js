'use strict';

import Repository from './Repository.js';
import ErrorBadRequest from '../errors/ErrorBadRequest.js';
import ErrorNotFound from '../errors/ErrorNotFound.js';

/**
 * Token repository in order to load, save and delete tokens from the database.
 */
export default class RepositoryToken extends Repository {

  /**
   * Returns all tokens from the database.
   * @return {Array} Array of all token objects.
   */
  async findAll() {
    const results = await this.query('SELECT * FROM token ORDER BY name ASC');
    return results.map(e => Object.assign({}, e));
  }

  /**
   * Finds a token in the database and returns it as Object.
   * @param  {Integer} id ID of the token in the database.
   * @return {Promise}    Returns a promis wich will evently
   * return the TokenEntity as Object.
   */
  async findById(id) {
    if (typeof id != 'number') 
      throw new ErrorBadRequest(
        "In order to find an token ID has to be a number, but '"
        + typeof id
        + "' was given."
      );

    const result = (await this.query(
      'SELECT * FROM token WHERE id=?',
      id
    )).map(e => Object.assign({}, e))[0];

    if(!result)
      throw new ErrorNotFound(
        "Token not found"
      );
    
    return result;
  }

  /**
   * Saves token to the database.
   * @param  {Object} token The token Object to save.
   * @return {Promise}      Promise which will eventually will 
   * give saved object.
   */
  save(token) {
    let _token = this.validateToken(token);

    if(_token.id === undefined){
      return this.#insert(_token);
    } else {
      return this.#update(_token);
    }
  }

  /**
   * Deletes a token record in the database.
   * @param  {Integer} id ID of token record to delete.
   * @return {Promise}    Promese gets resolved if delete is commplite.
   */
  deleteById(id) {
    if (typeof id != 'number') 
      throw new ErrorBadRequest(
        "In order to delete token id has to be a number, but '"
        + typeof id
        + "' was given."
      );
    
    return this.query(
      'DELETE FROM token WHERE id=?',
      id
    );
  }
  
  /**
   * Inserts token record in to database.
   * @param  {Object} token The token Object to insert.
   * @return {Promise}      Promise which will eventually will 
   * give saved object.
   */
  async #insert(token) {
    if (token.id !== undefined) 
      throw new ErrorBadRequest(
        "In order to insert token record token object can't have a id"
      );

    const results = await this.query(
      'INSERT INTO token SET ?',
      token
    );
      
    return this.findById(results.insertId);
  }

  /**
   * Updates token record in the database.
   * @param  {Object} token The token Object to update.
   * @return {Promise}      Promise which will eventually will 
   * give saved object.
   */
  async #update(token) {
    if (token.id === undefined) 
      throw new ErrorBadRequest(
        "In order to update token id has to be set."
      );

    const id = token.id;
    delete token.id // I don't need a clone here.

    const results = await this.query(
      'UPDATE token SET ? WHERE id=?',
      [token, id]
    );

    if(1 < results.results)
      throw new ErrorNotFound(
        "Could not find token record with ID '"
        + id + "'."
      );
      
    return this.findById(id);
  }

  /**
   * Valicates an token object and creats a clone.
   * @param  {Object} token token object to validate.
   * @return {Object}       Clone of token object.
   */
  validateToken(token) {
    if(typeof token != 'object')
      throw new ErrorBadRequest(
        "Token has to be an object, but '"
        + typeof token
        + "' was given."
      );

    if(typeof token.id != 'number' &&  token.id !== undefined)
      throw new ErrorBadRequest(
        "ID of token has to be a number, but '"
        + typeof token.id
        + "' was given."
      );

    if(typeof token.decimals != 'number' &&  token.decimals !== undefined)
      throw new ErrorBadRequest(
        "Decimals of token has to be a number, but '"
        + typeof token.decimals
        + "' was given."
      );

    if(typeof token.name != 'string' &&  token.name !== undefined)
      throw new ErrorBadRequest(
        "Name of token has to be a string, but '"
        + typeof token.name
        + "' was given."
      );

    if(typeof token.symbol != 'string' &&  token.symbol !== undefined)
      throw new ErrorBadRequest(
        "Symbol of token has to be a string, but '"
        + typeof token.symbol
        + "' was given."
      );

    if(typeof token.type != 'string' &&  token.type !== undefined)
      throw new ErrorBadRequest(
        "Type of token has to be a string, but '"
        + typeof token.type
        + "' was given."
      );

    if(typeof token.address != 'string' &&  token.address !== undefined)
      throw new ErrorBadRequest(
        "Address of token has to be a string, but '"
        + typeof token.address
        + "' was given."
      );

    if(typeof token.image != 'string' &&  token.image !== undefined)
      throw new ErrorBadRequest(
        "Image of token has to be a string, but '"
        + typeof token.image
        + "' was given."
      );

    if(typeof token.hidden != 'boolean' && typeof token.hidden != 'number' &&  token.hidden !== undefined)
      throw new ErrorBadRequest(
        "Hidden of token has to be a boolean, but '"
        + typeof token.hidden
        + "' was given."
      );

    //Insures nothing unexpected gets send to the database.
    const _token = { 
      id: token.id,
      name: token.name,
      symbol: token.symbol,
      type: token.type,
      address: token.address,
      decimals: token.decimals,
      image: token.image,
      hidden: token.hidden
    };

    //Clean up
    Object.keys(_token).forEach(
      key => _token[key] === undefined ? delete _token[key] : {}
    );

    return _token;
  }
}

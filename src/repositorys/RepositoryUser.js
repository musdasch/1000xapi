'use strict';

import Repository from './Repository.js';

/**
 * User repository in order to load, save and delete users from the database.
 */
export default class RepositoryUser extends Repository {

  /**
   * Returns all users from the database.
   * @return {Array} Array of all user objects.
   */
  async findAll() {
    const results = await this.query('SELECT * FROM user ORDER BY id ASC');
    return results.map(e => Object.assign({}, e));
  }

  /**
   * Finds an user in the database and returns it as Object.
   * @param  {String}   id   ID of the user;
   * @return {Object}        Returns user object.
   */
  async findById(id) {
    if (typeof id != 'number') 
      throw "In order to find an user by ID, ID has to be an number, but '"
        + typeof id
        + "' was given.";

    const result = (await this.query(
      'SELECT * FROM user WHERE id=?',
      id
    )).map(e => Object.assign({}, e))[0];

    return result;
  }

  /**
   * Finds an user in the database and returns it as Object.
   * @param  {String}   mail  Email of the user;
   * @return {Object}         Returns user object.
   */
  async findByEMail(mail) {
    if (typeof mail != 'string') 
      throw "In order to find an user by e-mail, mail has to be a string, but '"
        + typeof mail
        + "' was given.";

    const result = (await this.query(
      'SELECT * FROM user WHERE email=?',
      mail
    )).map(e => Object.assign({}, e))[0];

    return result;
  }

  /**
   * Saves User to the database.
   * @param  {Object} user  The user object to save.
   * @return {Object}       The user object.
   */
  save(user) {
    const _user = this.validateUser(user);

    if(_user.id === undefined){
      return this.#insert(_user);
    } else {
      return this.#update(_user);
    }
  }

  /**
   * Deletes an user record in the database.
   * @param  {Integer} id ID of user record to delete.
   * @return {Promise}    Promese gets resolved if delete is commplite.
   */
  deleteById(id) {
    if (typeof id != 'number') 
      throw "In order to delete user id has to be a number, but '"
        + typeof id
        + "' was given.";

    return this.query(
      'DELETE FROM user WHERE id=?',
      id
    );
  }

  /**
   * Inserts user record in to database.
   * @param  {Object} user  The user object to insert.
   * @return {Object}       The user object from the database.
   */
  async #insert(user) {
    if (user.id !== undefined) 
      throw "In order to insert user record user object can't have a id";

    const results = await this.query(
      'INSERT INTO user SET ?',
      user
    );
    
    return this.findById(results.insertId);
  }

  /**
   * Updates user record in the database.
   * @param  {Object} user  The user Object to update.
   * @return {Object}       The user Object form the database.
   * give saved object.
   */
  async #update(user) {
    if (user.id === undefined) 
      throw "In order to update user id has to be set.";

    const id = user.id;
    delete user.id // I don't need a clone here.

    const results = await this.query(
      'UPDATE user SET ? WHERE id=?',
      [user, id]
    );

    if(1 < results.results)
      throw "Could not find user record with ID '"
        + id + "'.";
      
    return this.findById(id);
  }

  /**
   * Valicates an user object and creats a clone.
   * @param  {Object} user  user object to validate.
   * @return {Object}       Clone of uset object.
   */
  validateUser(user) {
    if(typeof user != 'object')
      throw "User has to be an object, but '" + typeof user + "' was given.";

    if(typeof user.id != 'number' &&  user.id !== undefined)
      throw "ID of user has to be a number, but '"
        + typeof user.id
        + "' was given.test";

    if(typeof user.email != 'string' &&  user.email !== undefined)
      throw "Email of user has to be a string, but '"
        + typeof user.email
        + "' was given.";

    if(typeof user.password != 'string' &&  user.password !== undefined)
      throw "Password of user has to be a string, but '"
        + typeof user.password
        + "' was given.";

    //Insures nothing unexpected gets send to the database.
    const _user = { 
      id: user.id,
      email: user.email,
      password: user.password
    };

    //Clean up
    Object.keys(_user).forEach(
      key => _user[key] === undefined ? delete _user[key] : {}
    );

    return _user;
  }
}

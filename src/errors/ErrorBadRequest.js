'use strict';

/**
 * Error to handel bad requersts.
 */
export default class ErrorBadRequest extends Error {

	/**
	 * Constructor of the Error object.
	 * @param  {string} message Message to log.
	 */
	constructor(message) {
		super(message);
	}
}
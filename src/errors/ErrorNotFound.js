'use strict';

/**
 * Error class to henden not found errors.
 */
export default class ErrorNotFound extends Error {

	/**
	 * Constructor of the Error object.
	 * @param  {string} message Message to log.
	 */
	constructor(message) {
		super(message);
	}
}
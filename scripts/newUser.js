import RepositoryUser from '../src/repositorys/RepositoryUser.js';
import terminal  from 'terminal-kit';
import readJsonSync from 'read-json-sync';
import sha256 from 'sha256';

const config = readJsonSync('configs/config.json');
const term = terminal.terminal;

(async () => {
  const repository = new RepositoryUser(config.database);
  
  term.magenta("E-Mail: ");
  const email = await term.inputField().promise;
  term("\n");

  let password;
  let rePassword;

  do {
    term.magenta("Password: ");
    password = await term.inputField({echoChar: '•'}).promise;
    term("\n");

    term.magenta("Repeat password: ");
    rePassword = await term.inputField({echoChar: '•'}).promise;
    term("\n");

    if(password != '' && password == rePassword){
      await repository.save(
        {
          email: email,
          password: sha256(password).toUpperCase()
        }
      );

      term.green("\nSaved new user.\n\n");
    } else if(password == '') {
      term.red("\nPasswords empty.\n\n");
    } else {
      term.red("\nPasswords aren't identical. Please try again.\n\n");
    }
  } while (password == '' || password != rePassword);

  //Hack
  process.exit(1);
})();
import axios from 'axios';
import Web3 from 'web3';
import BigNumber from 'bignumber.js';
import readJsonSync from 'read-json-sync';
import RepositoryToken from './../src/repositorys/RepositoryToken.js';

const options = readJsonSync('configs/config.json');

const authBaseUrl = 'https://www.mintme.com/oauth/v2/';
const apiBaseUrl = 'https://www.mintme.com/dev/api/v2/';

const web3 = new Web3(
  new Web3.providers.HttpProvider(options.provider)
);

const account = web3.eth.accounts.privateKeyToAccount(
  options.botWalletPK
);
web3.eth.accounts.wallet.add(account);
web3.eth.defaultAccount = account.address;

const thousandXSWAPABI = readJsonSync("assets/abi/ThousandXSWAP.json");
const IERC20ABI = readJsonSync("assets/abi/IERC20.json");

const swapContract = new web3.eth.Contract(
  thousandXSWAPABI,
  options.thousandXSWAPAddress
);

const baseAddress = "0x7b535379bBAfD9cD12b35D91aDdAbF617Df902B2";
const fromContract = new web3.eth.Contract(
  IERC20ABI,
  baseAddress
);

/*
const repository = new RepositoryToken(options.database);
const tokens = await repository.findAll();
let checkList = [];
*/

const tradeValue = 0.05;
const addPrice = 0;

const checkList = [
  {
    id: 4282,
    name: "Anukis",
    address: "0xfcC19E279D0240cFdaBdEEB6885f6829FCCfa501"
  },
  {
    id: 7021,
    name: "BANG",
    address: "0xf8402c9E3218588EeEBb0CC08a01E841242bdEe0"
  },
  {
    id: 8672,
    name: "bitMonky",
    address: "0x3Eb5Ea03039450621500a7481525494c33d2aa0A"
  },
  {
    id: 4629,
    name: "CLICK",
    address: "0xf4811B341Af177bDE2407B976311Af66c4B08021"
  },
  {
    id: 3026,
    name: "CoronaCoin Extra",
    address: "0x6CDEb9BF6F73D36f721ab6EFBFA35fa003CEF563"
  },
  {
    id: 9412,
    name: "deZentrale",
    address: "0x40c8dF1653b5368e3dE8AC8f69F85BFAc7C5C5FE"
  },
  {
    id: 9907,
    name: "DogecoinX",
    address: "0x7cC70Ee464C78FAa09403bf18D93aE8C75A3cbc5"
  },
  {
    id: 14362,
    name: "GTPC",
    address: "0x7f0A21Dd46a87C3cEA64A36D771B13257b345324"
  },
  {
    id: 4514,
    name: "LIBAS",
    address: "0xd0DB84A63475Af5236Db6A1EEf1b2f777Dd77F38"
  },
  {
    id: 22,
    name: "MaTest",
    address: "0xEb2405953a528199aB626e0696d5B0cbAdB353fA"
  },
  {
    id: 10529,
    name: "MineMintToken",
    address: "0xA27c1AbD15bfFAAde6c2e873C10fc7a2beb72d69"
  },
  {
    id: 12493,
    name: "Ovelle",
    address: "0xcD950CbE4e34357997c8c60c599A47605Fcc18Bf"
  },
  {
    id: 9887,
    name: "PETETE",
    address: "0x35E4f986066a10023B32405E8B726A11f6042b38"
  },
  {
    id: 10104,
    name: "PRSS",
    address: "0xFe56deBf29Cab1a641543107e17c8d6225F9f626"
  },
  {
    id: 12051,
    name: "Prunity",
    address: "0x78CF733E6e113BA393b3bd17E4738E4dd63366fb"
  },
  {
    id: 6247,
    name: "Satoshi",
    address: "0xe73eE985605Ee470f1CE67d8a3DA0D6301e77389"
  },
  {
    id: 7,
    name: "SHELLS",
    address: "0xaa153ce997e1363cb31231e644c4266d9c954630"
  },
  {
    id: 7204,
    name: "Spiffcoin",
    address: "0x22b6bBbfFbf4D2eD3Da798e6f9E7a3c633585183"
  },
  {
    id: 23,
    name: "TREE",
    address: "0x69a3eDdB6bE2d56E668E7DfF68DB1303e675A0F0"
  },
  {
    id: 10783,
    name: "XatteR",
    address: "0xB580f1dbA1c17882Fca8f6DDadA8428c9cB177fC"
  }
];


(async () => {
  console.log(new Date().toString());

  try {
    const oauth = await axios.get(
      authBaseUrl + 'token',
      {
        params: options.mintme
      }
    );

    const headers = {
      'Content-Type': 'application/json',
      'Authorization':'Bearer ' + oauth.data.access_token
    }

    const userOrders = (await axios.get(
      apiBaseUrl + 'auth/user/orders/active',
      {
        params: {
          offset: 0,
          limit: 100
        },
        headers: headers
      }
    )).data;

    let limitRequestPuse = 1

    checkList.forEach(async check => {
      await new Promise((resolve, reject) => setTimeout(resolve, limitRequestPuse++ * 60000));
      const orders = userOrders.filter(
        order => check.id === order.market.base.id &&
        order.side === 1
      );

      if(1 > orders.length){
        const price = (await axios.get(
          apiBaseUrl + 'auth/orders/active',
          {
            params: {
              base: check.name,
              quote: "MINTME",
              offset: 0,
              limit: 1,
              side: "sell"
            },
            headers: headers
          }
        )).data[0].price;

        const value = await swapContract.methods
          .swapPreview(baseAddress, check.address, toInt(tradeValue, 12))
          .call();

        const valueDec = parseInt(toDecimal(value, 12));
        const myPrice = (parseInt(60/valueDec*1000)/1000)+0.001;

        if(myPrice < price){
          const toContract = new web3.eth.Contract(
            IERC20ABI,
            check.address
          );

          let gas = 0;
          const gasPrice = (await web3.eth.getGasPrice());

          gas = await fromContract.methods
              .approve(options.thousandXSWAPAddress, toInt(tradeValue, 12))
              .estimateGas();

          try {
          await fromContract.methods
              .approve(options.thousandXSWAPAddress, toInt(tradeValue, 12))
              .send({ 
                from: web3.eth.defaultAccount,
                gas: gas,
                gasPrice: gasPrice,
                nonce: (await web3.eth.getTransactionCount(web3.eth.defaultAccount))
              });
          } catch (e) {
            console.log(e);
          }

          gas = await swapContract.methods
            .swap(baseAddress, check.address, toInt(tradeValue, 12), value)
            .estimateGas();

          try {
          await swapContract.methods
            .swap(baseAddress, check.address, toInt(tradeValue, 12), value)
            .send({ 
              from: web3.eth.defaultAccount,
              gas: gas,
              gasPrice: gasPrice,
              nonce: (await web3.eth.getTransactionCount(web3.eth.defaultAccount))
            });
          } catch(e) {
            console.log(e);
          }

          gas = await toContract.methods
            .transfer('0x50bd504cc613f77b935866413ceaede9ea5476be', toInt(valueDec, 12))
            .estimateGas();

          try {
          await toContract.methods
            .transfer('0x50bd504cc613f77b935866413ceaede9ea5476be', toInt(valueDec, 12))
            .send({
              from: web3.eth.defaultAccount,
              gas: gas,
              gasPrice: gasPrice,
              nonce: (await web3.eth.getTransactionCount(web3.eth.defaultAccount))
            });
          } catch(e) {
            console.log(e);
          }

          setTimeout(
            placeOrder.bind(null, check.name, myPrice, valueDec),
            1000 * 60 * 15
          );
        }
      }
    });
    
  } catch(e) {
    console.log("Error:");
    console.log(e);
  }
})();

async function placeOrder(token, priceInput, amountInput) {
  try {
    const oauth = await axios.get(
      authBaseUrl + 'token',
      {
        params: options.mintme
      }
    );

    const headers = {
      'Content-Type': 'application/json',
      'Authorization':'Bearer ' + oauth.data.access_token
    }

    await axios.post(
      apiBaseUrl + 'auth/user/orders',
      {
        "base": token,
        "quote": "MINTME",
        "priceInput": priceInput,
        "amountInput": amountInput,
        "marketPrice": false,
        "action": "sell"
      },
      {
        headers: headers
      }
    );
    console.log(token + ":\t\t" + amountInput + "\t\t@\t" + priceInput);
  } catch(e) {
    console.log("Error:");
    console.log(e)
  }
}

function toInt(value, decimals) {
  const bigValue = new BigNumber(10);
  return bigValue.pow(decimals).times(value).toString(10);
}

function toDecimal(value, decimals) {
  const bigValue = new BigNumber(10);
  return bigValue.pow(-decimals).times(value).toNumber();
}

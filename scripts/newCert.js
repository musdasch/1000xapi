import selfsigned from 'selfsigned';
import jsonfile from 'jsonfile';


const attrs = [{ name: 'commonName', value: 'api.1000x.ch' }];
const pems = selfsigned.generate(attrs, { days: 365 });
const data = JSON.stringify(pems);

jsonfile.writeFile('assets/pems.json', data,  { spaces: 4 }, (err) => {
    if (err) {
        console.log(`Error writing file: ${err}`);
    } else {
        console.log(`Pems file is written successfully!`);
    }
});
# 1000x API
API for the 1000x page in order to manage the Tokens and record the transactions.

## Requrements
 - NodeJS
 - MariaDB
 - (optional) [core-geth](https://github.com/mintme-com/core-geth/releases) 

## Demo
 - [Demo Page](https://www.1000x.ch/)
 - You can easy test your API with [Insomnia](https://insomnia.rest/). you will find an export of an collection in the file `Insomnia.json`

## Project setup
Download the project from the GIT page.
```
git clone https://gitlab.com/musdasch/1000xapi.git
cd 1000xapi
```

Create two databases.
```
mysql -u [user] -p
CREATE DATABASE thousanddb_live;
CREATE DATABASE thousanddb_test;
exit;
```

Setup the config files.
```
cd configs/
cp config.json.example config.json
cp config.test.json.example config.test.json
cd ../
```

Set the user and the password for the database in the config files `configs/config.json` and `configs/config.test.json`.

```
{
  "database": {
    ...
    "user": "[user]",
    "password": "[password]",
    ...
  },
  ...
}
```

If you run your own geth node you also have to change the `provider`. 
```
{
  ...
  "provider": "localhost:8545",
  ...
}
```

Initialize the database.
```
npm run initDB
```

Add an user to the API server.
```
npm run newUser

> api.1000x@1.0.0 newUser
> node scripts/newUser.js

E-Mail: [email]
Password: [password]
Repeat password: [password]

Saved new user.
```

Create keys for your server.
```
npm run newCert


> api.1000x@1.0.0 newCert
> node scripts/newCert.js

Pems file is written successfully!
```

### Test server
```
npm test
```

### Start server
```
npm start
```

### Setup Systemd service
Add the user, group and working directory to `txapi.service`
```
...

[Service]
...
User=[user]
Group=[goupe]
WorkingDirectory=[folder of api server]

...
```

Setup service.
```
sudo cp txapi.service /etc/systemd/system/txapi.service
sudo chown root:root /etc/systemd/system/txapi.service
systemctl enable txapi.service
systemctl start txapi.service

systemctl status txapi.service
```

## License
Copyright © 2022 Musdasch <musdasch@protonmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

import assert from 'assert';
import ErrorBadRequest from '../../src/errors/ErrorBadRequest.js';

describe('ErrorBadRequest', function () {
  describe('#message', function () {
    it('should equal "test" when constructet with "test"', function () {
      const error = new ErrorBadRequest("test");
      assert.equal(error.message, "test");
    });
  });
});

import Server from '../src/servers/Server.js';

import ControllerAuth from '../src/controllers/ControllerAuth.js';

import RepositoryUser from '../src/repositorys/RepositoryUser.js';
import RepositoryToken from '../src/repositorys/RepositoryToken.js';

import ErrorNotFound from '../src/errors/ErrorNotFound.js';

import readJsonSync from 'read-json-sync';
import assert from 'assert';
import axios from 'axios';
import sha256 from 'sha256';

// Setup data
const config = readJsonSync('configs/config.test.json');
const baseUrl = 'http://localhost:' + config.server.port + '/';

const repositoryUser = new RepositoryUser(config.database);
const repositoryToken = new RepositoryToken(config.database);

const controllerAuth = new ControllerAuth(config);
let counter = 0;

/**
 * Setup a valid user for testing.
 * @return {String} JWT token.
 */
async function setupValidUser() {
  const user = {
    email: counter++ + "test@test.test",
    password: sha256("test").toUpperCase()
  };

  const reqUser = {
    email: "test@test.test",
    password: "test"
  };

  await repositoryUser.save(
    user
  );

  const res = await axios.post(
    baseUrl + 'login',
    reqUser
  );

  return res.data.token;
}


/**
 * Returns token in database
 * @return {Object} Token object from database.
 */
function setupToken() {
  return repositoryToken.save(
    {
      type: "ERC20",
      name: counter++ + "Test",
      address: "0x40c8dF1653b5368e3dE8AC8f69F85BFAc7C5C5FE",
      symbol: "TEST",
      decimals: 12,
      image: "https://www.1000x.ch/token_icons/test.png",
      hidden: false
    }
  );
}

(async () => {
  
  const server = new Server(
    config
  );
  await server.start();

  describe('Server', () => {
    before(() => {
      repositoryToken.query('DELETE FROM token');
      repositoryUser.query('DELETE FROM user');
    });

    after(() => {
      server.stop();
      repositoryToken.query('DELETE FROM token');
      repositoryUser.query('DELETE FROM user');
    });

    describe('start', () => {
      it('Sould response with 404', async () => {
        try {
          await axios.get(
            baseUrl,
          );
        } catch(e) {
          assert.equal(e.response.status, 404);
          assert.equal(e.response.data.message, 'Not Found');
          return;
        }
        assert.fail("Should not succeed");
      });
    });

    describe('POST /login', () => {
      it('Sould response jwt', async () => {
        const user = {
          email: "test@test.test",
          password: sha256("test").toUpperCase()
        };

        const reqUser = {
          email: "test@test.test",
          password: "test"
        };

        await repositoryUser.save(
          user
        );

        const res = await axios.post(
          baseUrl + 'login',
          reqUser
        );

        assert.equal(typeof res.data.token, 'string');
      });

      it('Sould response with valid jwt', async () => {
        const user = {
          email: "test@test.test",
          password: sha256("test").toUpperCase()
        };

        const reqUser = {
          email: "test@test.test",
          password: "test"
        };

        const dbUser = await repositoryUser.save(
          user
        );

        const res = await axios.post(
          baseUrl + 'login',
          reqUser
        );

        const token = await controllerAuth.verifyJWT(res.data.token);

        assert.equal(token.email, dbUser.email);
      });

      it('Sould response with 400', async () => {
        const user = {
          email: "test@test.test",
          password: sha256("test").toUpperCase()
        };

        const reqUser = {
          email: "test@test",
          password: "test"
        };

        await repositoryUser.save(
          user
        );

        try {
          await axios.post(
            baseUrl + 'login',
            reqUser
          );
        } catch(e) {
          assert.equal(e.response.status, 400);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 422', async () => {
        const user = {
          email: "test@test.test",
          password: sha256("test").toUpperCase()
        };

        const reqUser = {
          email: "test@test.test",
          password: "test2"
        };

        await repositoryUser.save(
          user
        );

        try {
          await axios.post(
            baseUrl + 'login',
            reqUser
          );
        } catch(e) {
          assert.equal(e.response.status, 422);
          return;
        }

        assert.fail("Should not succeed");
      });
    });

    describe('GET /tokens', () => {
      it('Sould response with 200', async () => {
        const res = await axios.get(
          baseUrl + 'tokens',
        );

        assert.equal(res.status, 200);
      });

      it('Sould response with []', async () => {
        const res = await axios.get(
          baseUrl + 'tokens',
        );

        assert.equal(typeof res.data, 'object');
        assert.equal(res.data.length, 0);
      });

      it('Sould response with array with one entry', async () => {
        const token = await repositoryToken.save(
          {
            type: "ERC20",
            name: "deZentrale",
            address: "40c8dF1653b5368e3dE8AC8f69F85BFAc7C5C5FE",
            symbol: "deZen",
            decimals: 12,
            image: "https://www.1000x.ch/token_icons/deZentrale.png",
            hidden: false
          }
        );

        const res = await axios.get(
          baseUrl + 'tokens',
        );

        assert.equal(typeof res.data, 'object');
        assert.equal(res.data.length, 1);

        const resToken = res.data[0];
        assert.equal(resToken.type, token.type);
        assert.equal(resToken.name, token.name);
        assert.equal(resToken.address, token.address);
        assert.equal(resToken.symbol, token.symbol);
        assert.equal(resToken.decimals, token.decimals);
        assert.equal(resToken.image, token.image);
        assert.equal(resToken.hidden, token.hidden);
      });
    });

    describe('GET /tokens/:id', () => {
      it('Sould response with 404', async () => {
        try {
          await axios.get(
            baseUrl + 'tokens/1',
          );
        } catch(e) {
          assert.equal(e.response.status, 404);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 400', async () => {
        try {
          await axios.get(
            baseUrl + 'tokens/a',
          );
        } catch(e) {
          assert.equal(e.response.status, 400);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 200', async () => {
        const token = await setupToken();

        const res = await axios.get(
          baseUrl + 'tokens/' + token.id,
        );

        assert.equal(res.status, 200);
      });

      it('Sould response with same object', async () => {
        const token = await setupToken();

        const res = await axios.get(
          baseUrl + 'tokens/' + token.id,
        );

        
        assert.equal(res.data.id, token.id);
        assert.equal(res.data.type, token.type);
        assert.equal(res.data.name, token.name);
        assert.equal(res.data.address, token.address);
        assert.equal(res.data.symbol, token.symbol);
        assert.equal(res.data.decimals, token.decimals);
        assert.equal(res.data.image, token.image);
        assert.equal(res.data.hidden, token.hidden);
      });
    });

    describe('POST /tokens', () => {
      it('Sould response with 403', async () => {
        try {
          await axios.post(
            baseUrl + 'tokens',
            {}
          );
        } catch(e) {
          assert.equal(e.response.status, 403);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 401', async () => {
        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer test'
          }
        }

        try {
          await axios.post(
            baseUrl + 'tokens',
            {},
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 401);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 400', async () => {
        const jwt = await setupValidUser();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        }

        try {
          await axios.post(
            baseUrl + 'tokens',
            {
              type: 1,
              name: "deZentrale",
              address: "40c8dF1653b5368e3dE8AC8f69F85BFAc7C5C5FE",
              symbol: "deZen",
              decimals: 12,
              image: "https://www.1000x.ch/token_icons/deZentrale.png"
            },
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 400);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 200', async () => {
        const jwt = await setupValidUser();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        }

        const res = await axios.post(
          baseUrl + 'tokens',
          {
            type: "ERC20",
            name: "deZentrale",
            address: "40c8dF1653b5368e3dE8AC8f69F85BFAc7C5C5FE",
            symbol: "deZen",
            decimals: 12,
            image: "https://www.1000x.ch/token_icons/deZentrale.png"
          },
          config
        );

        assert.equal(res.status, 200);
      });

      it('Sould response with response with same object with id', async () => {
        const jwt = await setupValidUser();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        }

        const token = {
          type: "ERC20",
          name: "deZentrale",
          address: "40c8dF1653b5368e3dE8AC8f69F85BFAc7C5C5FE",
          symbol: "deZen",
          decimals: 12,
          image: "https://www.1000x.ch/token_icons/deZentrale.png",
          hidden: true
        };

        const res = await axios.post(
          baseUrl + 'tokens',
          token,
          config
        );

        const resToken = res.data;

        assert.equal(typeof resToken.id, 'number');
        assert.equal(resToken.type, token.type);
        assert.equal(resToken.name, token.name);
        assert.equal(resToken.address, token.address);
        assert.equal(resToken.symbol, token.symbol);
        assert.equal(resToken.decimals, token.decimals);
        assert.equal(resToken.image, token.image);
        assert.equal(resToken.hidden, token.hidden);
      });
    });

    describe('PUT /tokens/:id', () => {
      it('Sould response with 403', async () => {
        try {
          await axios.put(
            baseUrl + 'tokens/1',
            {}
          );
        } catch(e) {
          assert.equal(e.response.status, 403);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 401', async () => {
        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer test'
          }
        }

        try {
          await axios.put(
            baseUrl + 'tokens/1',
            {},
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 401);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 400', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        }

        delete token.id

        try {
          await axios.put(
            baseUrl + 'tokens/a',
            token,
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 400);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 200', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();
        const reqToken = {...token};
        delete reqToken.id;

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        }

        const res = await axios.put(
          baseUrl + 'tokens/' + token.id,
          reqToken,
          config
        );

        assert.equal(res.status, 200);
      });

      it('Sould response with response with same object with id', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();
        
        const reqToken = {...token};
        delete reqToken.id;
        reqToken.name = 'Test2'

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        }

        const res = await axios.put(
          baseUrl + 'tokens/' + token.id,
          reqToken,
          config
        );

        const resToken = res.data;

        assert.equal(typeof resToken.id, 'number');
        assert.equal(resToken.type, reqToken.type);
        assert.equal(resToken.name, reqToken.name);
        assert.equal(resToken.address, reqToken.address);
        assert.equal(resToken.symbol, reqToken.symbol);
        assert.equal(resToken.decimals, reqToken.decimals);
        assert.equal(resToken.image, reqToken.image);
        assert.equal(resToken.hidden, reqToken.hidden);
      });
    });

    describe('PATCH /tokens/:id', () => {
      it('Sould response with 403', async () => {
        try {
          await axios.patch(
            baseUrl + 'tokens/1',
            {}
          );
        } catch(e) {
          assert.equal(e.response.status, 403);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 401', async () => {
        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer test'
          }
        }

        try {
          await axios.patch(
            baseUrl + 'tokens/1',
            {},
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 401);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 400', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        };

        delete token.id;
        delete token.type;
        delete token.name;
        delete token.address;
        delete token.symbol;
        delete token.hidden;

        try {
          await axios.patch(
            baseUrl + 'tokens/a',
            token,
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 400);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 200', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();
        
        const reqToken = {...token};
        delete reqToken.id;
        delete reqToken.type;
        delete reqToken.name;
        delete reqToken.address;
        delete reqToken.symbol;
        reqToken.decimals = 13;
        reqToken.image = "test";
        delete reqToken.hidden;

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        };

        const res = await axios.patch(
          baseUrl + 'tokens/' + token.id,
          reqToken,
          config
        );

        assert.equal(res.status, 200);
      });

      it('Sould response with same token as in database but changet valus', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();
        
        const reqToken = {...token};
        delete reqToken.id;
        delete reqToken.type;
        delete reqToken.name;
        delete reqToken.address;
        delete reqToken.symbol;
        reqToken.decimals = 13;
        reqToken.image = "test";
        delete reqToken.hidden;

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        };

        const res = await axios.patch(
          baseUrl + 'tokens/' + token.id,
          reqToken,
          config
        );

        const resToken = res.data;

        assert.equal(resToken.id, token.id);
        assert.equal(resToken.type, token.type);
        assert.equal(resToken.name, token.name);
        assert.equal(resToken.address, token.address);
        assert.equal(resToken.symbol, token.symbol);
        assert.equal(resToken.decimals, reqToken.decimals);
        assert.equal(resToken.image, reqToken.image);
        assert.equal(resToken.hidden, token.hidden);
      });
    });

    describe('DELETE /tokens/:id', () => {
      it('Sould response with 403', async () => {
        try {
          await axios.delete(
            baseUrl + 'tokens/1'
          );
        } catch(e) {
          assert.equal(e.response.status, 403);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 401', async () => {
        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer test'
          }
        }

        try {
          await axios.delete(
            baseUrl + 'tokens/1',
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 401);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 400', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        };

        try {
          await axios.delete(
            baseUrl + 'tokens/a',
            config
          );
        } catch(e) {
          assert.equal(e.response.status, 400);
          return;
        }

        assert.fail("Should not succeed");
      });

      it('Sould response with 204', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        };

        const res = await axios.delete(
          baseUrl + 'tokens/' + token.id,
          config
        );

        assert.equal(res.status, 204);
      });

      it('Sould be removed from repositorys', async () => {
        const jwt = await setupValidUser();
        const token = await setupToken();

        const config = {
          headers:{
            'Content-Type': 'application/json',
            'Authorization':'Bearer ' + jwt
          }
        };

        await axios.delete(
          baseUrl + 'tokens/' + token.id,
          config
        );

        try {
          await repositoryToken.findById(token.id);
        } catch (e) {
          assert.equal(e instanceof ErrorNotFound, true);
          assert.equal(e.message, 'Token not found');
          return;
        }
      });
    });

    describe('GET /trades', () => {
      it('Sould response with 200', async () => {
        const res = await axios.get(
          baseUrl + 'trades'
        );

        assert.equal(res.status, 200);
      });
    });
  });

  run();
})();

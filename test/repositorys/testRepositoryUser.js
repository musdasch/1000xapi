/*
import RepositoryUser from '../../src/repositorys/RepositoryUser.js';

import readJsonSync from 'read-json-sync';
import assert from 'assert';
import sha256 from 'sha256';

describe('RepositoryUser', () => {
  let repository;

  before(() => {
    const config = readJsonSync('configs/config.test.json');
    repository = new RepositoryUser(
      config.database
    );
  });
    
  afterEach(() => {
    repository.query('DELETE FROM user');
  });


  describe('#findAll()', () => {
    it(
      'Should equal empty array when first run',
      async () => {
        const users = await repository.findAll();
        assert.equal(users.length, 0);
      }
    );

    it(
      'Should equal array with on entry after save() when run',
      async () => {
        await repository.save(
          {
            email: "test@test.ch",
            password: sha256("test").toUpperCase()
          }
        );

        const users = await repository.findAll();
        assert.equal(users.length, 1);
        assert.equal(
          users[0].email,
          "test@test.ch"
        );
        assert.equal(
          users[0].password,
          sha256("test").toUpperCase()
        );
      }
    );
  });

  describe('#save()', () => {
    it(
      'Return object should hold same data',
      async () => {
        const user = await repository.save(
          {
            email: "test@test.ch",
            password: sha256("test").toUpperCase()
          }
        );
        assert.equal(
          user.email,
          "test@test.ch"
        );
        assert.equal(
          user.password,
          sha256("test").toUpperCase()
        );
      }
    );

    it(
      'ID of return object should be a number',
      async () => {
        const user = await repository.save(
          {
            email: "test@test.ch",
            password: sha256("test").toUpperCase()
          }
        );
        assert.equal(typeof user.id, 'number');
      }
    );

  });
});
*/